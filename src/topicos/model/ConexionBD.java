/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package topicos.model;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author gmendez
 */
public class ConexionBD {

    public com.mysql.jdbc.Connection conn;
    public com.mysql.jdbc.Statement stat;
    public ResultSet res;
    variablesDB var;

    public ConexionBD() {
        var = new variablesDB();
        var.setDireccion("localhost");
        var.setPuerto("3306");
        var.setUsuario("root");
        var.setContra("1234");
        var.setDb("musica");
    }

    public void conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");                         //Direccion //Puerto                                    //Usuario //Conra
            this.conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+ var.getDireccion()+":"+var.getPuerto()+"/"+ var.getDb()+"?zeroDateTimeBehavior=convertToNull", ""+ var.getUsuario() +"", "" +var.getContra() +"");
            //System.out.println("Conexión exitosa");
            this.stat = (Statement) this.conn.createStatement();
        } catch (Exception e) {
            System.out.println("Error en iniciar conexión: " + e.getMessage());
        }
    }
}
